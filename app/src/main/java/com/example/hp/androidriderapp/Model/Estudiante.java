package com.example.hp.androidriderapp.Model;

public class Estudiante {
    private String career, email, fec_nac, image, name, password, phone;

    public Estudiante() {
    }

    public Estudiante(String career, String email, String fec_nac, String image, String name, String password, String phone) {
        this.career = career;
        this.email = email;
        this.fec_nac = fec_nac;
        this.image = image;
        this.name = name;
        this.password = password;
        this.phone = phone;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFec_nac() {
        return fec_nac;
    }

    public void setFec_nac(String fec_nac) {
        this.fec_nac = fec_nac;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
