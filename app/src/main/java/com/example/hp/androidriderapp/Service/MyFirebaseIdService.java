package com.example.hp.androidriderapp.Service;

import com.example.hp.androidriderapp.Common.Common;
import com.example.hp.androidriderapp.Model.Token;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseIdService extends FirebaseInstanceIdService{
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        updateTokenToServer(refreshedToken);// cuando tenga un token de actualización, debemos actualizar a nuestra base de datos en tiempo real

    }

    private void updateTokenToServer(String refreshedToken) {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference tokens = db.getReference(Common.token_tbl);

        Token token = new Token(refreshedToken);
        if(Common.ses_estudiante != null)// Si ya inició sesión, debe actualizar el Token
            tokens.child(Common.ses_pasajero_id)
                    .setValue(token);
    }
}
