package com.example.hp.androidriderapp.Model;

public class Pasajero {
    private String fec_registro;

    public Pasajero() {
    }

    public Pasajero(String fec_registro) {
        this.fec_registro = fec_registro;
    }

    public String getFec_registro() {
        return fec_registro;
    }

    public void setFec_registro(String fec_registro) {
        this.fec_registro = fec_registro;
    }
}
