package com.example.hp.androidriderapp;



import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.example.hp.androidriderapp.Common.Common;
import com.example.hp.androidriderapp.Model.Estudiante;
import com.example.hp.androidriderapp.Model.Pasajero;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.Map;

import dmax.dialog.SpotsDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginPasajeroActivity extends AppCompatActivity {

    Button btnSignIn;
    RelativeLayout rootLayout;

    FirebaseDatabase db;
    DatabaseReference estudiantes, pasajeros;

    private final static int PERMISSION = 1000;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Arkhip_font.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_main);

        db = FirebaseDatabase.getInstance();
        estudiantes = db.getReference(Common.estudiante_tbl);
        pasajeros = db.getReference(Common.pasajero_tbl);
        rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);

        // Inicializar vista
        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoginDialog();
            }
        });
    }

    private void showLoginDialog() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("INICIA SESIÓN ");
        alertDialog.setMessage("Por favor, usa tu usuario para iniciar sesión");

        LayoutInflater inflater = this.getLayoutInflater();
        View login_layout = inflater.inflate(R.layout.layout_signin, null);

        final MaterialEditText edtUsuario = login_layout.findViewById(R.id.edtUsuario);
        final MaterialEditText edtPass = login_layout.findViewById(R.id.edtPassword);

        alertDialog.setView(login_layout);

        // Configurar el botón
        alertDialog.setPositiveButton("INGRESAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Chequea validación
                if (TextUtils.isEmpty(edtUsuario.getText().toString())) {
                    Snackbar.make(rootLayout, "Ingresa tu Usuario", Snackbar.LENGTH_SHORT).show(); return;
                }
                if (TextUtils.isEmpty(edtPass.getText().toString())) {
                    Snackbar.make(rootLayout, "Ingresa tu clave", Snackbar.LENGTH_SHORT).show(); return;
                }
                if (edtPass.getText().toString().length() < 6) {
                    Snackbar.make(rootLayout, "La Clave es muy corta", Snackbar.LENGTH_SHORT).show(); return;
                }

                // Deshabilitar el botón SIGN IN mientras está procesando

                final android.app.AlertDialog waitingDialog = new SpotsDialog(LoginPasajeroActivity.this);
                waitingDialog.show();

                // LOGIN
                Query query = estudiantes.orderByKey().equalTo(edtUsuario.getText().toString());
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                        System.out.println("dataSnapshot.getValue() "+dataSnapshot.getValue());
                        if (dataSnapshot.getValue() != null) {
                            String pass = dataSnapshot.child(edtUsuario.getText().toString()+"/password").getValue().toString();
                            if (pass.equals(edtPass.getText().toString())) {
                                // VERIFICAR SI EXISTE EN "PASAJERO", SI NO EXISTE LO REGISTRAMOS
                                Query query = pasajeros.orderByKey().equalTo(edtUsuario.getText().toString());
                                query.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.getValue() == null) {
                                            pasajeros.child(edtUsuario.getText().toString()).setValue(new Pasajero(""));
                                        }
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                                Common.ses_estudiante = dataSnapshot.child(edtUsuario.getText().toString()).getValue(Estudiante.class);
                                Common.ses_pasajero_id = edtUsuario.getText().toString();
                                startActivity(new Intent(LoginPasajeroActivity.this, Home.class));
                                finish();
                            } else {
                                Snackbar.make(rootLayout, "Clave incorrecta", Snackbar.LENGTH_SHORT).show();
                            }
                        } else {
                            Snackbar.make(rootLayout, "Usuario incorrecto", Snackbar.LENGTH_SHORT).show();
                        }
                        waitingDialog.dismiss();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        waitingDialog.dismiss();
                    }
                });
            }
        });

        alertDialog.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                dialogInterface.dismiss();
            }
        });

        alertDialog.show();
    }
}
