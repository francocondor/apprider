package com.example.hp.androidriderapp.Common;

import com.example.hp.androidriderapp.Model.Estudiante;
import com.example.hp.androidriderapp.Remote.FCMClient;
import com.example.hp.androidriderapp.Remote.IFCMService;

public class Common {
    public static final String viaje_tbl = "Viaje";
    public static final String conductor_tbl = "Conductor";
    public static final String pasajero_tbl = "Pasajero";
    public static final String solicitudViaje_tbl = "SolicitudViaje";
    public static final String estudiante_tbl = "Estudiante";
    public static final String token_tbl = "Tokens";

    public static Estudiante ses_estudiante;
    public static String ses_pasajero_id = null;

    public static final String fcmURL = "https://fcm.googleapis.com/";

    public static IFCMService getFCMService()
    {
        return FCMClient.getClient(fcmURL).create(IFCMService.class);
    }
}
