package com.example.hp.androidriderapp.Model;

public class Conductor {
    private String fec_registro, lic_conducir;

    public Conductor() {
    }

    public Conductor(String fec_registro, String lic_conducir) {
        this.fec_registro = fec_registro;
        this.lic_conducir = lic_conducir;
    }

    public String getFec_registro() {
        return fec_registro;
    }

    public void setFec_registro(String fec_registro) {
        this.fec_registro = fec_registro;
    }

    public String getLic_conducir() {
        return lic_conducir;
    }

    public void setLic_conducir(String lic_conducir) {
        this.lic_conducir = lic_conducir;
    }
}
